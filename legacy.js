/*-------------------------------------------------------------------------
    
    tumblr: legacy lightbox initializer
    - gitlab.com/quick-tumblr-lightbox/q
    - written by HT (@glenthemes)
    
-------------------------------------------------------------------------*/

window.quick_legacy_lightbox = function(o_o){
  let selector = o_o.trim().replaceAll(", ",",");
  let sel_array = selector.split(",");
  
  // enable new object creation
  function legacyIMG(w, h, ld, hd){
    this.width = Number(w);
    this.height = Number(h);
    this.low_res = ld;
    this.high_res = hd;
  }
  
  sel_array.forEach(sel => {
    if(document.querySelector(sel) !== null){
        document.querySelectorAll(sel).forEach(sett => {
            let aerie = [];

            sett.querySelectorAll("img").forEach((imgz, index) => {
                aerie.push(new legacyIMG(
                    imgz.getAttribute("width"),
                    imgz.getAttribute("height"),
                    imgz.src,
                    imgz.src
                ));

                let nonZeroIndex = Math.floor(parseInt(index)+1);
                imgz.setAttribute("index",nonZeroIndex)
                
                imgz.addEventListener("click", () => {
                    sett.setAttribute("onclick","Tumblr.Lightbox.init(" + JSON.stringify(aerie) + "," + nonZeroIndex + ")")
                })
            })
        })//end selector forEach
    }//end: if user's selector (param) exists
    
  })//end text forEach
}//end function
