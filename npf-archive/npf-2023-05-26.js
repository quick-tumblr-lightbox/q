/*-------------------------------------------------------------------------
    
    tumblr: npf lightbox initializer
    - written by HT (@glenthemes)
    
    requirements (recommended):
    Instead of {block:Posts}, use:
    {block:Posts inlineMediaWidth="1280" inlineNestedMediaWidth="1280"}
    
-------------------------------------------------------------------------*/

window.quick_npf_lightbox = function(o_o){
  let selector = o_o.trim().replaceAll(", ",",");
  let sel_array = selector.split(",");
  
  // enable new object creation
  function npfIMG(w, h, ld, hd){
    this.width = Number(w);
    this.height = Number(h);
    this.low_res = ld;
    this.high_res = ld;
  }
  
  sel_array.forEach(sel => {
    if(document.querySelector(sel) !== null){
      document.querySelectorAll(sel).forEach(npf_set => {
        let aerie = [];
        
        if(npf_set.querySelector("a.post_media_photo_anchor img") == null){
          let tmblr_full = ".tmblr-full";
          if(npf_set.querySelector(tmblr_full) !== null){
            npf_set.querySelectorAll(tmblr_full).forEach(tf => {
              
              let anchor = document.createElement("a");
              anchor.classList.add("post_media_photo_anchor");
              
              let mini_w = tf.querySelector("img").getAttribute("data-orig-width");
              let mini_h = tf.querySelector("img").getAttribute("data-orig-height");
              let mini_src = tf.querySelector("img").src;
              
              anchor.setAttribute("data-big-photo-width",mini_w);
              anchor.setAttribute("data-big-photo-height",mini_h);
              anchor.setAttribute("data-big-photo",mini_src)
              
              tf.appendChild(anchor);
              anchor.appendChild(tf.querySelector("img"))
            })//end: .tmblr-full forEach
          }//end: if .tmblr-full exists
        }//end: if a.anchor DOESN'T exist, use .tmblr-full
        
        // if a.anchor exists, do the thing
        if(npf_set.querySelector("a.post_media_photo_anchor img") !== null){
            
            npf_set.querySelectorAll(".post_media_photo_anchor").forEach((a_link, index) => {
              aerie.push(new npfIMG(
                  a_link.getAttribute("data-big-photo-width"),
                  a_link.getAttribute("data-big-photo-height"),
                  a_link.getAttribute("data-big-photo"),
                  a_link.getAttribute("data-big-photo")
              ));

              let nonZeroIndex = Math.floor(parseInt(index)+1);

              // remove the auto-lightbox on single npf images
              // (originally on a.post_media_photo_anchor)
              a_link.removeAttribute("data-big-photo");

              a_link.setAttribute("img-index",nonZeroIndex)

              a_link.addEventListener("click", () => {
                  npf_set.setAttribute("onclick","Tumblr.Lightbox.init(" + JSON.stringify(aerie) + "," + nonZeroIndex + ")")
                
                // shake the screen to wake up lightbox image
                // ok I'm just being dramatic but sometimes the 1st pic won't show
                if(nonZeroIndex == 1){
                  setTimeout(() => {
                    let winEvt;
                    if(typeof(Event) === "function"){
                      winEvt = new Event("resize");
                    } else {
                      /*IE*/
                      winEvt = document.createEvent("Event");
                      winEvt.initEvent("resize", true, true);
                    }
                    window.dispatchEvent(winEvt);
                  },0)
                }                
              })
            })//end: a.post_media_photo_anchor forEach
        }//end: initiate lightbox on a.post_media_photo_anchor
        
      })//end: npf_set forEach
    }//end: if user's selector (param) exists    
    
  })//end text forEach
}//end function
