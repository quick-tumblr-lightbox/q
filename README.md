### quick lightbox plugin for tumblr photosets

**Description:** enable (or bring back) the lightbox feature for legacy or npf photosets  
**Author:** HT (@&#x200A;glenthemes)

---

#### Features / Limitations:
* This plugin only provides the lightbox functionality. If you need additional features, like custom photoset spacing, this plugin does not provide that.

#### Prerequisites / Requirements:
* It is expected that you're familiar with your theme's HTML markup.
* Whether you're using this for legacy photosets or NPF photosets, each of them need to be neatly wrapped in `div` of some sort. By default, Tumblr doesn't group neighboring NPF images for you.

---

Initialize the script. You can choose to paste the following in these locations:
* after `<head>`, before `<style>` (this seems to work the most)
* after `</style>`, before `</head>`
* after `<body>`, before `</body>`
```html
<!--✻✻✻✻✻✻  QUICK TUMBLR LIGHTBOXES by @glenthemes  ✻✻✻✻✻✻-->
<script src="//quick-tumblr-lightbox.gitlab.io/q/npf.js"></script>
<script src="//quick-tumblr-lightbox.gitlab.io/q/legacy.js"></script>
<script>
document.addEventListener("DOMContentLoaded", () => {
    quick_npf_lightbox(".your_npf_photosets_wrapper");
    quick_legacy_lightbox(".your_legacy_photosets_wrapper");
});
</script>
```

---

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)


#### Thank you!
If this helped you, please consider leaving a tip:  
:coffee:&ensp;[ko-fi.com/glenthemes](https://ko-fi.com/glenthemes)
